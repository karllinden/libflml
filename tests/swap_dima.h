/*
 * This file is part of flml.
 *
 * Copyright (C) 2021-2022 Karl Linden <karl.j.linden@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef TEST_DIMA_H
#define TEST_DIMA_H

#include <dima/proxy.h>

/**
 * A DIMA implementation that merely invokes the next DIMA, and allows the next
 * implementation to be swapped with run_with_swapped_dima.
 */
struct swap_dima {
    struct dima_proxy proxy;
    struct dima *next;
};

void swap_dima_init(struct swap_dima *dima, struct dima *next);

static inline struct dima *swap_dima_to_dima(struct swap_dima *dima) {
    return dima_from_proxy(&dima->proxy);
}

/**
 * Run the given function with the next DIMA temporarily swapped to the given.
 */
void run_with_swapped_dima(struct swap_dima *dima,
                           struct dima *next,
                           void (*fun)(void *),
                           void *ptr);

#endif /* !TEST_DIMA_H */
