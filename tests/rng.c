/*
 * This file is part of flml.
 *
 * Copyright (C) 2018-2019, 2021-2022 Karl Linden <karl.j.linden@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

#include "rng.h"

#define RAND48_MAX 2147483647 /* 2^31 - 1, see man drand48 */

struct seedstate {
    unsigned short seed[3];
    unsigned char i; /* 0, 1, 2 */
    unsigned char j; /* 0, 1 */
};

static unsigned short rng_hex_to_ushort(const char hex[4]) {
    unsigned u = 0;
    for (unsigned i = 0; i < 4; ++i) {
        u <<= 4;
        if (hex[i] >= 'A' && hex[i] <= 'F') {
            u |= 10 + hex[i] - 'A';
        } else if (hex[i] >= 'a' && hex[i] <= 'f') {
            u |= 10 + hex[i] - 'a';
        } else if (hex[i] >= '0' && hex[i] <= '9') {
            u |= hex[i] - '0';
        } else {
            fprintf(stderr, "error: invalid char %c in hex %4s\n", hex[i], hex);
            abort();
        }
    }
    return (unsigned short)u;
}

static void rng_hex_to_seed(const char hex[13], unsigned short seed[3]) {
    seed[0] = rng_hex_to_ushort(hex);
    seed[1] = rng_hex_to_ushort(hex + 4);
    seed[2] = rng_hex_to_ushort(hex + 8);
}

static void rng_ushort_to_hex(unsigned short u, char hex[4]) {
    for (int i = 3; i >= 0; --i) {
        char c = u & 0xf;
        if (c < 10) {
            hex[i] = c + '0';
        } else {
            hex[i] = c - 10 + 'a';
        }
        u >>= 4;
    }
}

static void rng_seed_to_hex(const unsigned short seed[3], char hex[13]) {
    rng_ushort_to_hex(seed[0], hex);
    rng_ushort_to_hex(seed[1], hex + 4);
    rng_ushort_to_hex(seed[2], hex + 8);
    hex[12] = '\0';
}

static void rng_seed_exact(unsigned short seed[3]) {
    char hex[13];
    rng_seed_to_hex(seed, hex);
    fprintf(stderr, "info: seeding random number generator with %s\n", hex);
    seed48(seed);
}

static void rng_seed_mix(struct seedstate *statep, void *data, size_t size) {
    /* Not optimized, but performance is not critical here. */
    unsigned char *c = data;
    for (size_t i = 0; i < size; ++i) {
        unsigned short us = c[i];
        if (statep->j) {
            us <<= 8;
        }
        statep->seed[statep->i] ^= us;

        statep->j++;
        if (statep->j == 2) {
            statep->j = 0;
            statep->i++;
            if (statep->i == 3) {
                statep->i = 0;
            }
        }
    }
    return;
}

static void rng_generate_seed(unsigned short seed[3]) {
    struct seedstate state;
    struct timespec ts;
    clock_t clk;
    gid_t gid;
    pid_t pid;
    pid_t ppid;
    uid_t uid;

    memset(&state, 0, sizeof(state));

    clock_gettime(CLOCK_REALTIME, &ts);
    clk = clock();
    gid = getgid();
    pid = getpid();
    ppid = getppid();
    uid = getuid();

    rng_seed_mix(&state, &clk, sizeof(clk));
    rng_seed_mix(&state, &ts, sizeof(ts));
    rng_seed_mix(&state, &gid, sizeof(gid));
    rng_seed_mix(&state, &pid, sizeof(pid));
    rng_seed_mix(&state, &ppid, sizeof(ppid));
    rng_seed_mix(&state, &uid, sizeof(uid));

    memcpy(seed, state.seed, 3 * sizeof(unsigned short));
}

void rng_seed(void) {
    const char *value = getenv("RNG_SEED");
    unsigned short seed[3];

    if (!value) {
        /* Generate a new seed. */
        rng_generate_seed(seed);
    } else if (strlen(value) == 12) {
        /* Use the supplied seed. */
        rng_hex_to_seed(value, seed);
    } else {
        /* Weep. */
        fprintf(stderr,
                "error: expected the seed %s to be 12 "
                "characters, but it was %zd\n",
                value,
                strlen(value));
        abort();
    }

    rng_seed_exact(seed);
}

long rng_max(long max) {
    long maxmul = RAND48_MAX;
    long r;

    assert(max >= 0);

    /* Inclusive. */
    max++;

    /* Generate unbiased, so maxmul must be a multiple of max. */
    maxmul -= maxmul % max;

    do {
        r = lrand48();
    } while (r >= maxmul);
    return r % max;
}

long rng_min_max(long min, long max) {
    return min + rng_max(max - min);
}
