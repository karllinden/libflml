/*
 * This file is part of flml.
 *
 * Copyright (C) 2018-2019, 2021-2022 Karl Linden <karl.j.linden@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef _FLML_ERROR_H_
#define _FLML_ERROR_H_

#include <stdio.h>
#include <time.h>

/**
 * The type of an error from FLML.
 */
enum flml_error_type {
    /**
     * No error.
     */
    FLML_ERROR_NONE,

    /**
     * An error occured while allocating memory.
     *
     * If the default memory allocating functions are used, this means there is
     * insufficient memory.
     */
    FLML_ERROR_MEM,

    /**
     * A computation would overflow.
     */
    FLML_ERROR_OVERFLOW,

    /**
     * An error occurred while setting up the signal handling functionality of
     * the main loop.
     */
    FLML_ERROR_SIGNAL,

    /**
     * An error occurred during a library or system call.
     *
     * In this case the name of the function failing, together with an errno
     * code is given in the system member of struct flml_error.
     */
    FLML_ERROR_SYSTEM,
};

/**
 * Data structure for FLML_ERROR_SYSTEM.
 */
struct flml_error_system {
    /**
     * The name of the library or system call failing.
     */
    const char *call;

    /**
     * The errno code.
     */
    int err;
};

/**
 * Structure containing a description of an error that has occurred.
 */
struct flml_error {
    /**
     * The type of the error that occurred.
     */
    enum flml_error_type type;

    /**
     * The union containing the data.
     */
    union {
        /**
         * Data when type is FLML_ERROR_SYSTEM.
         */
        struct flml_error_system system;
    };
};

/**
 * Prints human readable error information to the given string.
 *
 * This function does not write more than size bytes to the string (including
 * the terminating null byte). If the output was truncated due to this limit,
 * then the return value is the number of characters (excluding the terminal
 * null byte) which would have been written to the final string if enough space
 * had been available. Thus, a return value of size or more means that the
 * output was truncated.
 *
 * @param str the string to write error information to
 * @param size the maximum size of the written string including the terminating
 *             null byte
 * @param error a pointer to the error whose information is to be printed
 * @return the number of characters in the resulting string (excluding the
 *         terminal null byte) on success, or a negative number on failure
 */
int flml_error_snprint(char *str, size_t size, const struct flml_error *error);

/**
 * Prints human readable error information to the given file.
 *
 * @param file the file
 * @param error a pointer to the whose information is to be printed
 * @return the number of characters printed on success, or a negative
 *         number on failure
 */
int flml_error_fprint(FILE *file, const struct flml_error *error);

#endif /* !_FLML_ERROR_H_ */
